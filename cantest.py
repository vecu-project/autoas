import os
from random import randint
import time

import tkinter 


# 102 : Cluster data
# XXXX XXXX XX XX XXXX
# ---- ---- -- -- ----
#   |    |   |  |   L reserved
#   |    |   |  L Fuel
#   |    |   L Temperature                
#   |    L TachoSpeed
#   L  VehicleSpeed

# 106 : otherData
#  X  X  X  X  X  X  X  X  X  X  X  X    XXXX
#  -  -  -  -  -  -  -  -  -  -  -  -    ----
# (1)(2)(3)(4)(5)(6)(7)(8)(9)(10)(11)(12) L(13) reserved

# (1) reserverd;
# (2) bool AirbagState;
# (4) bool AutoCruiseState;
# (3) bool HighBeamState;
# (6) bool LowOilState;
# (5) bool PosLampState;
# (8) bool SeatbeltDriverState;
# (7) bool SeatbeltPassengerState;
# (10) bool TPMSState;
# (9) bool TurnLeftState;
# (12) bool TurnRightState;
        

CanMsg102 = '0000000000000000'
CanMsg106 = '0000000000000000'

os.system(f"cansend can0 102#{CanMsg102}")


root = tkinter.Tk()
root.geometry("550x600")
SpeedStr = tkinter.StringVar()
FuelStr = tkinter.StringVar()

def Accelerator(param):
    global CanMsg102
    global CanMsg106
    
    if param == 'Up':
        CanMsg106 = CanMsg106[0:3] + '0' + CanMsg106[4:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        AutoCruise.config(image = off)
        
        CanMsg102 = f'0001000101010000'
        os.system(f"cansend can0 102#{CanMsg102}")
        print(f"cansend can0 102#{CanMsg102}")
        SpeedStr.set("N Km/h")
    elif param == 'Down':
        AutoCruise.config(image = off)
        CanMsg102 = f'0002000202010000'
        os.system(f"cansend can0 102#{CanMsg102}")
        print(f"cansend can0 102#{CanMsg102}")
        SpeedStr.set("N Km/h")
    elif param == 'Stop':
        CanMsg102 = f'0000000000000000'
        os.system(f"cansend can0 102#{CanMsg102}")
        print(f"cansend can0 102#{CanMsg102}")
        SpeedStr.set("0 Km/h")
    elif param == 'Fuel':
        CanMsg102 = f'0000000000020000'
        os.system(f"cansend can0 102#{CanMsg102}")
        print(f"cansend can0 102#{CanMsg102}")
        FuelStr.set("Fuel Status")

SpeedStr.set("0 Km/h")
FuelStr.set("Full")
SpeedStatus = tkinter.Label(root, textvariable=SpeedStr, bg='white', fg='black', font = ("Helvetica",  14, 'bold'), borderwidth=2, relief="sunken", width='20', height='2')
SpeedUp = tkinter.Button(root, text="Acceleration", command=lambda: Accelerator('Up'), width='10')
SpeedDown = tkinter.Button(root, text="Deceleration", command=lambda: Accelerator('Down'), width='10')
SpeedStop = tkinter.Button(root, text="Stop", command=lambda: Accelerator('Stop'), width='10')
SpeedStatus.grid(row=0, column=0)

SpeedUp.grid(row=0, column=1)
SpeedDown.grid(row=0, column=2)
SpeedStop.grid(row=0, column=3)

FuelStatus = tkinter.Label(root, textvariable=FuelStr, bg='white', fg='black', font = ("Helvetica",  14, 'bold'), borderwidth=2, relief="sunken", width='20', height='2')


FuelUp = tkinter.Button(root, text="Fuel Up", command=lambda: Accelerator('Fuel'), width='10')
FuelStatus.grid(row=1, column=0)
FuelUp.grid(row=1, column=1)



AirbagStr = tkinter.StringVar()
AutoCruiseStr = tkinter.StringVar()
HighBeamStr = tkinter.StringVar()
LowOilStr = tkinter.StringVar()
PosLampStr = tkinter.StringVar()
SeatbeltDriverStr = tkinter.StringVar()
SeatbeltPassengerStr = tkinter.StringVar()
TpmsStr = tkinter.StringVar()
TurnLeftStr = tkinter.StringVar()
TurnRightStr = tkinter.StringVar()
AirbagIsOn = False
AutoCruiseIsOn = False
HighBeamIsOn = False
LowOilIsOn = False
PosLampIsOn = False
SeatbeltDriverIsOn = False
SeatbeltPassengerIsOn = False
TpmsIsOn = False
TurnLeftIsOn = False
TurnRightIsOn = False

def AirbagSwitch():
    global AirbagIsOn
    global CanMsg106
        
    if not AirbagIsOn:
        Airbag.config(image = on)
        AirbagIsOn = True
        AirbagStr.set('False')
        CanMsg106 = CanMsg106[0:1] + 'f' + CanMsg106[2:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif AirbagIsOn:
        Airbag.config(image = off)
        AirbagIsOn = False
        AirbagStr.set('True')
        CanMsg106 = CanMsg106[0:1] + '0' + CanMsg106[2:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")

def AutoCruiseSwitch():
    global AutoCruiseIsOn
    global CanMsg106
    
    if not AutoCruiseIsOn:
        AutoCruise.config(image = on)
        AutoCruiseIsOn = True
        AutoCruiseStr.set('False')
        CanMsg106 = CanMsg106[0:3] + 'f' + CanMsg106[4:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
        CanMsg102 = '0003000303010000'
        os.system(f"cansend can0 102#{CanMsg102}")
    elif AutoCruiseIsOn:
        AutoCruise.config(image = off)
        AutoCruiseIsOn = False
        AutoCruiseStr.set('True')
        CanMsg106 = CanMsg106[0:3] + '0' + CanMsg106[4:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
        
def HighBeamSwitch():
    global HighBeamIsOn
    global CanMsg106
    
    if not HighBeamIsOn:
        HighBeam.config(image = on)
        HighBeamIsOn = True
        HighBeamStr.set('False')
        CanMsg106 = CanMsg106[0:2] + 'f' + CanMsg106[3:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif HighBeamIsOn:
        HighBeam.config(image = off)
        HighBeamIsOn = False
        HighBeamStr.set('True')
        CanMsg106 = CanMsg106[0:2] + '0' + CanMsg106[3:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")

def LowOilSwitch():
    global LowOilIsOn
    global CanMsg106
    
    if not LowOilIsOn:
        LowOil.config(image = on)
        LowOilIsOn = True
        LowOilStr.set('False')
        CanMsg106 = CanMsg106[0:5] + 'f' + CanMsg106[6:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif LowOilIsOn:
        LowOil.config(image = off)
        LowOilIsOn = False
        LowOilStr.set('True')
        CanMsg106 = CanMsg106[0:5] + '0' + CanMsg106[6:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")



def PosLampSwitch():
    global PosLampIsOn
    global CanMsg106
    
    if not PosLampIsOn:
        PosLamp.config(image = on)
        PosLampIsOn = True
        PosLampStr.set('False')
        CanMsg106 = CanMsg106[0:4] + 'f' + CanMsg106[5:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
        
    elif PosLampIsOn:
        PosLamp.config(image = off)
        PosLampIsOn = False
        PosLampStr.set('True')
        CanMsg106 = CanMsg106[0:4] + '0' + CanMsg106[5:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")

def SeatbeltDriverSwitch():
    global SeatbeltDriverIsOn
    global CanMsg106
    
    if not SeatbeltDriverIsOn:
        SeatbeltDriver.config(image = on)
        SeatbeltDriverIsOn = True
        SeatbeltDriverStr.set('False')
        CanMsg106 = CanMsg106[0:7] + 'f' + CanMsg106[8:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif SeatbeltDriverIsOn:
        SeatbeltDriver.config(image = off)
        SeatbeltDriverIsOn = False
        SeatbeltDriverStr.set('True')
        CanMsg106 = CanMsg106[0:7] + '0' + CanMsg106[8:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")


def SeatbeltPassengerSwitch():
    global SeatbeltPassengerIsOn
    global CanMsg106
    
    if not SeatbeltPassengerIsOn:
        SeatbeltPassenger.config(image = on)
        SeatbeltPassengerIsOn = True
        SeatbeltPassengerStr.set('False')
        CanMsg106 = CanMsg106[0:6] + 'f' + CanMsg106[7:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif SeatbeltPassengerIsOn:
        SeatbeltPassenger.config(image = off)
        SeatbeltPassengerIsOn = False
        SeatbeltPassengerStr.set('True')
        CanMsg106 = CanMsg106[0:6] + '0' + CanMsg106[7:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")

        
def TpmsSwitch():
    global TpmsIsOn
    global CanMsg106
    
    if not TpmsIsOn:
        Tpms.config(image = on)
        TpmsIsOn = True
        TpmsStr.set('False')
        CanMsg106 = CanMsg106[0:9] + 'f' + CanMsg106[10:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif TpmsIsOn:
        Tpms.config(image = off)
        TpmsIsOn = False
        TpmsStr.set('True')
        CanMsg106 = CanMsg106[0:9] + '0' + CanMsg106[10:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
        
def TurnLeftSwitch():
    global TurnLeftIsOn
    global CanMsg106
    
    if not TurnLeftIsOn:
        TurnLeft.config(image = on)
        TurnRight.config(image = off)
        TurnLeftIsOn = True
        TurnLeftStr.set('False')
        if CanMsg106[11] == 'f':
            CanMsg106 = CanMsg106[0:11] + '0' + CanMsg106[12:16]
            os.system(f"cansend can0 106#{CanMsg106}")
            time.sleep(1)
            CanMsg106 = CanMsg106[0:8] + 'f' + CanMsg106[9:16]
        else:
            CanMsg106 = CanMsg106[0:8] + 'f' + CanMsg106[9:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif TurnLeftIsOn:
        TurnLeft.config(image = off)
        TurnLeftIsOn = False
        TurnLeftStr.set('True')
        CanMsg106 = CanMsg106[0:8] + '0' + CanMsg106[9:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")

def TurnRightSwitch():
    global TurnRightIsOn
    global CanMsg106
    
    if not TurnRightIsOn:
        TurnLeft.config(image = off)
        TurnRight.config(image = on)
        
        TurnRightIsOn = True
        TurnRightStr.set('False')
        if CanMsg106[8] == 'f':
            CanMsg106 = CanMsg106[0:8] + '0' + CanMsg106[9:16]
            os.system(f"cansend can0 106#{CanMsg106}")
            time.sleep(1)
            CanMsg106 = CanMsg106[0:11] + 'f' + CanMsg106[12:16]
        else:
            CanMsg106 = CanMsg106[0:11] + 'f' + CanMsg106[12:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")
    elif TurnRightIsOn:
        TurnRight.config(image = off)
        TurnRightIsOn = False
        TurnRightStr.set('True')
        CanMsg106 = CanMsg106[0:11] + '0' + CanMsg106[12:16]
        os.system(f"cansend can0 106#{CanMsg106}")
        print(f"cansend can0 106#{CanMsg106}")

CanMsg106 = '0000000000000000'
os.system(f"cansend can0 106#{CanMsg106}")

on = tkinter.PhotoImage(file = "on.png")
off = tkinter.PhotoImage(file = "off.png")


# 0f00000000000000
# 000f000000000000
# 00f0000000000000
# 00000f0000000000
# 0000f00000000000
# 0000000f00000000
# 000000f000000000
# 000000000f000000
# 00000000f0000000
# 00000000000f0000

AirbagState = tkinter.Label(root, text='Airbag', fg='black', font = ("Helvetica",  14, 'bold'), width='20')
Airbag = tkinter.Button(root, image = off, bd = 0, command = AirbagSwitch, width='100')
AirbagState.grid(row=2, column=0)
Airbag.grid(row=2, column=1)

AutoCruiseState = tkinter.Label(root, text='AutoCruise', fg='black', font = ("Helvetica",  14, 'bold'), width='20')
AutoCruise = tkinter.Button(root, image = off, bd = 0, command = AutoCruiseSwitch, width='100')
AutoCruiseState.grid(row=3, column=0)
AutoCruise.grid(row=3, column=1)

HighBeamState = tkinter.Label(root, text='HighBeam', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
HighBeam = tkinter.Button(root, image = off, bd = 0, command = HighBeamSwitch, width='100')
HighBeamState.grid(row=4, column=0)
HighBeam.grid(row=4, column=1)

LowOilState = tkinter.Label(root, text='LowOil', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
LowOil = tkinter.Button(root, image = off, bd = 0, command = LowOilSwitch, width='100')
LowOilState.grid(row=5, column=0)
LowOil.grid(row=5, column=1)

PosLampState = tkinter.Label(root, text='PosLamp', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
PosLamp = tkinter.Button(root, image = off, bd = 0, command = PosLampSwitch, width='100')
PosLampState.grid(row=6, column=0)
PosLamp.grid(row=6, column=1)

SeatbeltDriverState = tkinter.Label(root, text='SeatbeltDriver', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
SeatbeltDriver = tkinter.Button(root, image = off, bd = 0, command = SeatbeltDriverSwitch, width='100')
SeatbeltDriverState.grid(row=7, column=0)
SeatbeltDriver.grid(row=7, column=1)

SeatbeltPassengerState = tkinter.Label(root, text='SeatbeltPassenger', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
SeatbeltPassenger = tkinter.Button(root, image = off, bd = 0, command = SeatbeltPassengerSwitch, width='100')
SeatbeltPassengerState.grid(row=8, column=0)
SeatbeltPassenger.grid(row=8, column=1)

TpmsState = tkinter.Label(root, text='Tpms', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
Tpms = tkinter.Button(root, image = off, bd = 0, command = TpmsSwitch, width='100')
TpmsState.grid(row=9, column=0)
Tpms.grid(row=9, column=1)

TurnLeftState = tkinter.Label(root, text='TurnLeft', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
TurnLeft = tkinter.Button(root, image = off, bd = 0, command = TurnLeftSwitch, width='100')
TurnLeftState.grid(row=10, column=0)
TurnLeft.grid(row=10, column=1)

TurnRightState = tkinter.Label(root, text='TurnRight', fg='black', font = ("Helvetica",  14, 'bold'), width='20', height='2')
TurnRight = tkinter.Button(root, image = off, bd = 0, command = TurnRightSwitch, width='100')
TurnRightState.grid(row=11, column=0)
TurnRight.grid(row=11, column=1)

# HarzardSigState = tkinter.Button(root, text="Harzard Signal")



root.mainloop()


