/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2018  AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
/* ============================ [ INCLUDES  ] ====================================================== */
#include "Rte_Gauge.h"
#ifdef USE_STMO
#include "Stmo.h"
#endif

#ifdef USE_COM
#include "Com.h"
#endif
/* ============================ [ MACROS    ] ====================================================== */
/* ============================ [ TYPES     ] ====================================================== */
/* ============================ [ DECLARES  ] ====================================================== */
/* ============================ [ DATAS     ] ====================================================== */
/* ============================ [ LOCALS    ] ====================================================== */
#ifdef USE_STMO
static void sample_pointer(void)
{
#ifdef COM_SID_VehicleSpeed
	uint16 VehicleSpeed = 0;
#endif
#ifdef COM_SID_TachoSpeed
	uint16 TachoSpeed = 0;
#endif
#ifdef COM_SID_Temperature
	uint8 Temperature = 0;
#endif
#ifdef COM_SID_Fuel
	uint8 Fuel = 0;
#endif

	static Stmo_DegreeType tacho = 0;
	static Stmo_DegreeType speed = 0;
	static Stmo_DegreeType temp = 0;
	static Stmo_DegreeType fuel = 9000;
	static boolean tacho_up = TRUE;
	static boolean speed_up = TRUE;
	static boolean temp_up = TRUE;
	static boolean fuel_up = TRUE;

#ifdef COM_SID_TachoSpeed

	(void)Com_ReceiveSignal(COM_SID_TachoSpeed, &TachoSpeed);
	// printf("TachoSpeed : %x\n", TachoSpeed);
	if (TachoSpeed == 1 && tacho <= 24000) { tacho += 10; }
	else if (TachoSpeed == 2 && tacho > 50) { tacho -= 10; }
	else if (TachoSpeed == 3) { tacho = tacho; }
	else if (TachoSpeed == 0) { tacho = 0; }

	#ifdef COM_SID_TxTachoSpeed
	Com_SendSignal(COM_SID_TxTachoSpeed, &tacho);
	#endif
#else
	if (tacho_up)
	{
		tacho += 50;
		if (tacho >= 24000)
		{
			tacho = 24000;
			tacho_up = FALSE;
		}
	}
	else
	{
		if (tacho > 100)
		{
			tacho -= 100;
		}
		else
		{
			tacho = 0;
			tacho_up = TRUE;
		}
	}
#endif

#ifdef COM_SID_VehicleSpeed
	(void)Com_ReceiveSignal(COM_SID_VehicleSpeed, &VehicleSpeed);
	if (VehicleSpeed == 1 && speed <= 24000)  { speed += 10; }
	else if (VehicleSpeed == 2 && speed > 50) { speed -= 10; }
	else if (VehicleSpeed == 3) { speed = speed; }
	else if (VehicleSpeed == 0) { speed = 0; }

	#ifdef COM_SID_TxVehicleSpeed
	Com_SendSignal(COM_SID_TxVehicleSpeed, &speed);
	#endif
	// printf("VehicleSpeed : %x\n", VehicleSpeed);
#else
	if (speed_up)
	{
		speed += 50;
		if (speed >= 24000)
		{
			speed = 24000;
			speed_up = FALSE;
		}
	}
	else
	{
		if (speed > 100)
		{
			speed -= 100;
		}
		else
		{
			speed = 0;
			speed_up = TRUE;
		}
	}
#endif

#ifdef COM_SID_Temperature
	(void)Com_ReceiveSignal(COM_SID_Temperature, &Temperature);
	// printf("Temperature: %x\n", Temperature);
	// temp = Temperature * 37;
	if (Temperature == 1 && temp <= 9700)  { temp += 5; }
	else if (Temperature == 2 && temp > 50) { temp -= 5; }
	else if (Temperature == 3) { temp = temp; }
	else if (Temperature == 0) { temp = 0; }

	#ifdef COM_SID_TxTemperature
	Com_SendSignal(COM_SID_TxTemperature, &temp);
	#endif
#else
	if (temp_up)
	{
		temp += 50;
		if (temp >= 9700)
		{
			temp = 9700;
			temp_up = FALSE;
		}
	}
	else
	{
		if (temp > 50)
		{
			temp -= 50;
		}
		else
		{
			temp = 0;
			temp_up = TRUE;
		}
	}
#endif

#ifdef COM_SID_Fuel
	(void)Com_ReceiveSignal(COM_SID_Fuel, &Fuel);
	// printf("Fuel: %x\n", Fuel);
	// fuel = Fuel * 37;
	if (Fuel == 1 && fuel <= 9000 && fuel > 30)  { fuel -= 1; }
	else if (Fuel == 2 && fuel > 50 && fuel <= 9000) { fuel += 10; }
	else if (Fuel == 3) { fuel = fuel; }
	else if (Fuel == 0) { fuel = fuel; }

	#ifdef COM_SID_TxFuel
	Com_SendSignal(COM_SID_TxFuel, &fuel);
	#endif

#else
	if (fuel_up)
	{
		fuel += 1;
		if (fuel >= 9700)
		{
			fuel = 9700;
			fuel_up = FALSE;
		}
	}
	else
	{
		if (fuel > 50)
		{
			fuel -= 1;
		}
		else
		{
			fuel = 0;
			fuel_up = TRUE;
		}
	}
#endif
	Stmo_SetPosDegree(STMO_ID_SPEED, speed);
	Stmo_SetPosDegree(STMO_ID_TACHO, tacho);
	Stmo_SetPosDegree(STMO_ID_TEMP, temp);
	Stmo_SetPosDegree(STMO_ID_FUEL, fuel);
}
#endif
/* ============================ [ FUNCTIONS ] ====================================================== */
void Gauge_Run(void)
{
#ifdef USE_STMO
	sample_pointer();
#endif
}
