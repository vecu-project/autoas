#ifndef RTE_TYPE_H
#define RTE_TYPE_H

/*********************************************************************************************************************
* Includes
*********************************************************************************************************************/
#include "Std_Types.h"

/*********************************************************************************************************************
* Data Type Definitions
*********************************************************************************************************************/

#define Rte_TypeDef_UINT16_T
typedef uint16 UINT16_T;
#define UINT16_T_LowerLimit ((UINT16_T)0u)
#define UINT16_T_UpperLimit ((UINT16_T)65535u)

#define Rte_TypeDef_UINT32_T
typedef uint32 UINT32_T;
#define UINT32_T_LowerLimit ((UINT32_T)0u)
#define UINT32_T_UpperLimit ((UINT32_T)4294967295u)

#define Rte_TypeDef_UINT8_T
typedef uint8 UINT8_T;
#define UINT8_T_LowerLimit ((UINT8_T)0u)
#define UINT8_T_UpperLimit ((UINT8_T)255u)


#ifndef RTE_SUPPRESS_UNUSED_DATATYPES

typedef boolean Boolean;

typedef sint16 SInt16;
#define SInt16_LowerLimit ((SInt16)-32768)
#define SInt16_UpperLimit ((SInt16)32767)

typedef sint32 SInt32;
#define SInt32_LowerLimit ((SInt32)-2147483648)
#define SInt32_UpperLimit ((SInt32)2147483647)

typedef sint8 SInt8;
#define SInt8_LowerLimit ((SInt8)-128)
#define SInt8_UpperLimit ((SInt8)127)

typedef uint16 UInt16;
#define UInt16_LowerLimit ((UInt16)0u)
#define UInt16_UpperLimit ((UInt16)65535u)

typedef uint32 UInt32;
#define UInt32_LowerLimit ((UInt32)0u)
#define UInt32_UpperLimit ((UInt32)4294967295u)

typedef uint8 UInt8;
#define UInt8_LowerLimit ((UInt8)0u)
#define UInt8_UpperLimit ((UInt8)255u)

#endif

#endif //RTE_TYPE_H

