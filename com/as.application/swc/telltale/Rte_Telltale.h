#ifndef RTE_TELLTALE_H
#define RTE_TELLTALE_H

#include "Rte.h"
#include "Rte_Type.h"

/*********************************************************************************************************************
* Init Values
*********************************************************************************************************************/
#define Rte_InitValue_Telltale_TPMSState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_LowOilState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_PosLampState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_TurnLeftState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_TurnRightState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_AutoCruiseState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_HighBeamState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_SeatbeltDriverState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_SeatbeltPassengerState ((OnOff_T)3u)
#define Rte_InitValue_Telltale_AirbagState ((OnOff_T)3u)

/*********************************************************************************************************************
* API Prototypes
*********************************************************************************************************************/
Std_ReturnType Rte_Write_Telltale_Telltale_AirbagState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_AutoCruiseState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_HighBeamState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_LowOilState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_PosLampState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_SeatbeltDriverState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_SeatbeltPassengerState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_TPMSState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_TurnLeftState(OnOff_T data);
Std_ReturnType Rte_Write_Telltale_Telltale_TurnRightState(OnOff_T data);

/*********************************************************************************************************************
* Rte_Write_<p>_<d>
*********************************************************************************************************************/
#define Rte_Write_Telltale_AirbagState Rte_Write_Telltale_Telltale_AirbagState
#define Rte_Write_Telltale_AutoCruiseState Rte_Write_Telltale_Telltale_AutoCruiseState
#define Rte_Write_Telltale_HighBeamState Rte_Write_Telltale_Telltale_HighBeamState
#define Rte_Write_Telltale_LowOilState Rte_Write_Telltale_Telltale_LowOilState
#define Rte_Write_Telltale_PosLampState Rte_Write_Telltale_Telltale_PosLampState
#define Rte_Write_Telltale_SeatbeltDriverState Rte_Write_Telltale_Telltale_SeatbeltDriverState
#define Rte_Write_Telltale_SeatbeltPassengerState Rte_Write_Telltale_Telltale_SeatbeltPassengerState
#define Rte_Write_Telltale_TPMSState Rte_Write_Telltale_Telltale_TPMSState
#define Rte_Write_Telltale_TurnLeftState Rte_Write_Telltale_Telltale_TurnLeftState
#define Rte_Write_Telltale_TurnRightState Rte_Write_Telltale_Telltale_TurnRightState
/*********************************************************************************************************************
* Runnable Telltale_run
*********************************************************************************************************************/
void Telltale_run(void);


#endif //RTE_TELLTALE_H

