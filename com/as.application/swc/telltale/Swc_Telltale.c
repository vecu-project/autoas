/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2017 AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
/* ============================ [ INCLUDES  ] ====================================================== */
#include "Rte_Telltale.h"
/* ============================ [ MACROS    ] ====================================================== */
/* ============================ [ TYPES     ] ====================================================== */
/* ============================ [ DECLARES  ] ====================================================== */
/* ============================ [ DATAS     ] ====================================================== */
/* ============================ [ LOCALS    ] ====================================================== */
/* ============================ [ FUNCTIONS ] ====================================================== */
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "Lcd.h"

#ifdef USE_COM
#include "Com.h"
#endif
//#include "/home/okdongja/autosar/autoas/aaaa.h"
//#include "/home/okdongja/autosar/autoas/com/as.infrastructure/arch/posix/mcal/Lcd1.h"

// #ifdef USE_posix

#define ON true
#define OFF false

#define bool boolean

#ifdef USE_COM

#ifdef COM_SID_AirbagState
Boolean airbagState = false;
#endif

#ifdef COM_SID_AutoCruiseState
Boolean autoCruiseState = false;
#endif

#ifdef COM_SID_HighBeamState
Boolean highBeamState = false;
#endif

#ifdef COM_SID_LowOilState
Boolean lowOilState = false;
#endif

#ifdef COM_SID_PosLampState
Boolean posLampState = false;
#endif

#ifdef COM_SID_SeatbeltDriverState
Boolean seatbeltDriverState = false;
#endif

#ifdef COM_SID_SeatbeltPassengerState
Boolean seatbeltPassengerState = false;
#endif

#ifdef COM_SID_TPMSState
Boolean tpmsState = false;
#endif

#ifdef COM_SID_TurnLeftState
Boolean turnLeftState = false;
#endif

#ifdef COM_SID_TurnRightState
Boolean turnRightState = false;
#endif

#ifdef COM_SID_HarzardLightState
Boolean harzardLightState = false;
#endif

#else

extern bool AirbagState;
extern bool AutoCruiseState;
extern bool HighBeamState;
extern bool LowOilState;
extern bool PosLampState;
extern bool SeatbeltDriverState;
extern bool SeatbeltPassengerState;
extern bool TPMSState;
extern bool TurnLeftState;
extern bool TurnRightState;

#endif

// #endif

#include <unistd.h>

int VAL_CAL(uint8 val)
{
	if (val == 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Telltale_run(void)
{ 

#ifdef USE_COM

#ifdef COM_SID_AirbagState
	uint8 airbag = 0;
	(void)Com_ReceiveSignal(COM_SID_AirbagState, &airbag);
	airbagState = VAL_CAL(airbag);
	// // printf("airbag : %x\n", airbag);

	if (airbagState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_AirbagState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_AirbagState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_AirbagState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_AirbagState(OnOff_Off);
	#endif
	}
	// if (airbagState == true)
	// {
	// 	// printf("airbagState : %s\n", airbagState ? "true" : "false");
	// }
	#ifdef COM_SID_TxAirbagState
	Com_SendSignal(COM_SID_TxAirbagState, &airbag);
	#endif
#endif

#ifdef COM_SID_AutoCruiseState

	uint8 autoCruise = 0;
	(void)Com_ReceiveSignal(COM_SID_AutoCruiseState, &autoCruise);
	autoCruiseState = VAL_CAL(autoCruise);
	// printf("autoCruise : %x\n", autoCruise);
	if (autoCruiseState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_AutoCruiseState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_AutoCruiseState(OnOff_On);
	#endif
	}
	else
	{

	#ifdef USE_posix
			Rte_Write_Telltale_AutoCruiseState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_AutoCruiseState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxAutoCruiseState
	Com_SendSignal(COM_SID_TxAutoCruiseState, &autoCruise);
	#endif
	// if (autoCruiseState == true)
	// 	// printf("autoCruiseState : %s\n", autoCruiseState ? "true" : "false");
#endif

#ifdef COM_SID_HighBeamState
	uint8 highBeam = 0;
	(void)Com_ReceiveSignal(COM_SID_HighBeamState, &highBeam);
	highBeamState = VAL_CAL(highBeam);
	// printf("highBeam : %x\n", highBeam);
	if (highBeamState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_HighBeamState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_HighBeamState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_HighBeamState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_HighBeamState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxHighBeamState
	Com_SendSignal(COM_SID_TxHighBeamState, &highBeam);
	#endif
	// if (highBeamState == true)
	// 	// printf("highBeamState : %s\n", highBeamState ? "true" : "false");
#endif

#ifdef COM_SID_LowOilState
	uint8 lowOil = 0;
	(void)Com_ReceiveSignal(COM_SID_LowOilState, &lowOil);
	lowOilState = VAL_CAL(lowOil);
	// printf("lowOil : %x\n", lowOil);
	if (lowOilState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_LowOilState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_LowOilState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_LowOilState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_LowOilState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxLowOilState
	Com_SendSignal(COM_SID_TxLowOilState, &lowOil);
	#endif
	// if (lowOilState == true)
	// 	// printf("lowOilState : %s\n", lowOilState ? "true" : "false");
#endif

#ifdef COM_SID_PosLampState
	uint8 posLamp = 0;
	(void)Com_ReceiveSignal(COM_SID_PosLampState, &posLamp);
	posLampState = VAL_CAL(posLamp);
	// printf("posLamp : %x\n", posLamp);
	if (posLampState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_PosLampState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_PosLampState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_PosLampState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_PosLampState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxPosLampState
	Com_SendSignal(COM_SID_TxPosLampState, &posLamp);
	#endif
	// if (posLampState == true)
	// 	// printf("posLampState : %s\n", posLampState ? "true" : "false");

#endif
#ifdef COM_SID_SeatbeltDriverState
	uint8 seatbeltDriver = 0;
	(void)Com_ReceiveSignal(COM_SID_SeatbeltDriverState, &seatbeltDriver);
	seatbeltDriverState = VAL_CAL(seatbeltDriver);
	// printf("seatbeltDriver : %x\n", seatbeltDriver);
	if (seatbeltDriverState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_SeatbeltDriverState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_SeatbeltDriverState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_SeatbeltDriverState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_SeatbeltDriverState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxSeatbeltDriverState
	Com_SendSignal(COM_SID_TxSeatbeltDriverState, &seatbeltDriver);
	#endif
	// if (seatbeltDriverState == true)
	// 	// printf("seatbeltDriverState : %s\n", seatbeltDriverState ? "true" : "false");

#endif

#ifdef COM_SID_SeatbeltPassengerState
	uint8 seatbeltPassenger = 0;
	(void)Com_ReceiveSignal(COM_SID_SeatbeltPassengerState, &seatbeltPassenger);
	seatbeltPassengerState = VAL_CAL(seatbeltPassenger);
	// printf("seatbeltPassenger : %x\n", seatbeltPassenger);

	if (seatbeltPassengerState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_SeatbeltPassengerState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_SeatbeltPassengerState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_SeatbeltPassengerState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_SeatbeltPassengerState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxSeatbeltPassengerState
	Com_SendSignal(COM_SID_TxSeatbeltPassengerState, &seatbeltPassenger);
	#endif

	// if (seatbeltPassengerState == true)
	// 	// printf("seatbeltPassengerState : %s\n", seatbeltPassengerState ? "true" : "false");
#endif

#ifdef COM_SID_TPMSState
	uint8 tpms = 0;
	(void)Com_ReceiveSignal(COM_SID_TPMSState, &tpms);
	tpmsState = VAL_CAL(tpms);
	// if(highBeamState==true) 
	// printf("tpms : %x\n", tpms);

	if (tpmsState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_TPMSState(RTE_CONST_OnOff_On);
	#elif USE_versatilepb
			Rte_Write_Telltale_TPMSState(OnOff_On);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_TPMSState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_TPMSState(OnOff_Off);
	#endif
	}
	// if (tpmsState == true)
	// 	// printf("tpmsState : %s\n", tpmsState ? "true" : "false");
	#ifdef COM_SID_TxTPMSState
	Com_SendSignal(COM_SID_TxTPMSState, &tpms);
	#endif

#endif



#ifdef COM_SID_TurnLeftState
	uint8 turnLeft = 0;
	(void)Com_ReceiveSignal(COM_SID_TurnLeftState, &turnLeft);
	turnLeftState = VAL_CAL(turnLeft);
	// if(turnLeft==true) 
	// printf("turnLeft : %x\n", turnLeft);

	if (turnLeftState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_1Hz);
			Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_TurnLeftState(OnOff_1Hz);
			Rte_Write_Telltale_TurnRightState(OnOff_Off);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_TurnLeftState(OnOff_Off);
	#endif
	}
	#ifdef COM_SID_TxTurnLeftState
	Com_SendSignal(COM_SID_TxTurnLeftState, &turnLeft);
	#endif
	// if (turnLeftState == true)
	// 	// printf("turnLeftState : %s\n", turnLeftState ? "true" : "false");
#endif


#ifdef COM_SID_TurnRightState
	uint8 turnRight = 0;
	(void)Com_ReceiveSignal(COM_SID_TurnRightState, &turnRight);
	turnRightState = VAL_CAL(turnRight);
	// if(highBeamState==true) 
	// printf("turnRight : %x\n", turnRight);

	if (turnRightState == ON)
	{
	#ifdef USE_posix
			Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_1Hz);
			Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_TurnRightState(OnOff_Off);
			Rte_Write_Telltale_TurnLeftState(OnOff_1Hz);
	#endif
	}
	else
	{
	#ifdef USE_posix
			Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_Off);
	#elif USE_versatilepb
			Rte_Write_Telltale_TurnRightState(OnOff_Off);
	#endif
	}
	// if (turnRightState == true)
	// 	// printf("turnRightState : %s\n", turnRightState ? "true" : "false");
	#ifdef COM_SID_TxTurnRightState
	Com_SendSignal(COM_SID_TxTurnRightState, &turnRight);
	#endif
#endif

#else
	if (AirbagState == ON)
	{
		Rte_Write_Telltale_AirbagState(RTE_CONST_OnOff_On);
	}
	else if (AirbagState == OFF)
	{
		Rte_Write_Telltale_AirbagState(RTE_CONST_OnOff_Off);
	}

	if (AutoCruiseState == ON)
	{
		Rte_Write_Telltale_AutoCruiseState(RTE_CONST_OnOff_On);
	}
	else if (AutoCruiseState == OFF)
	{
		Rte_Write_Telltale_AutoCruiseState(RTE_CONST_OnOff_Off);
	}

	if (HighBeamState == ON)
	{
		Rte_Write_Telltale_HighBeamState(RTE_CONST_OnOff_On);
	}
	else if (HighBeamState == OFF)
	{
		Rte_Write_Telltale_HighBeamState(RTE_CONST_OnOff_Off);
	}

	if (LowOilState == ON)
	{
		Rte_Write_Telltale_LowOilState(RTE_CONST_OnOff_On);
	}
	else if (LowOilState == OFF)
	{
		Rte_Write_Telltale_LowOilState(RTE_CONST_OnOff_Off);
	}

	if (PosLampState == ON)
	{
		Rte_Write_Telltale_PosLampState(RTE_CONST_OnOff_On);
	}
	else if (PosLampState == OFF)
	{
		Rte_Write_Telltale_PosLampState(RTE_CONST_OnOff_Off);
	}

	if (SeatbeltDriverState == ON)
	{
		Rte_Write_Telltale_SeatbeltDriverState(RTE_CONST_OnOff_On);
	}
	else if (SeatbeltDriverState == OFF)
	{
		Rte_Write_Telltale_SeatbeltDriverState(RTE_CONST_OnOff_Off);
	}

	if (SeatbeltPassengerState == ON)
	{
		Rte_Write_Telltale_SeatbeltPassengerState(RTE_CONST_OnOff_On);
	}
	else if (SeatbeltPassengerState == OFF)
	{
		Rte_Write_Telltale_SeatbeltPassengerState(RTE_CONST_OnOff_Off);
	}

	if (TPMSState == ON)
	{
		Rte_Write_Telltale_TPMSState(RTE_CONST_OnOff_On);
	}
	else if (TPMSState == OFF)
	{
		Rte_Write_Telltale_TPMSState(RTE_CONST_OnOff_Off);
	}

	if (TurnLeftState == ON)
	{
		Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_1Hz);
		Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_Off);
	}
	else if (TurnLeftState == OFF)
	{
		Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_Off);
	}

	if (TurnRightState == ON)
	{
		Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_1Hz);
		Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_Off);
	}
	else if (TurnRightState == OFF)
	{
		Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_Off);
	}
#endif

	// #endif
	// Rte_Write_Telltale_QQQQQQQQQQQQQQQQQQQState(RTE_CONST_OnOff_Off);

	// #ifdef USE_versatilepb

	// #endif
}
