
/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2015  AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
 
#ifndef SGRES_H
#define SGRES_H

#define __SG_WIDTH__ 1200
#define __SG_HEIGHT__ 500

#define __SG_PIXEL__ 1

#define SGR_BG_PNG                           0
extern const SgSRC sgwClusterBackground_SRC;
#define SGR_POINTER_BIG_PNG                  0
extern void* RefreshClusterTachoPointer(SgWidget* w);
extern void CacheClusterTachoPointer(SgWidget* w);
extern const SgSRC sgwClusterTachoPointer_SRC;
#define SGR_POINTER_BIG_PNG                  0
extern void* RefreshClusterSpeedPointer(SgWidget* w);
extern void CacheClusterSpeedPointer(SgWidget* w);
extern const SgSRC sgwClusterSpeedPointer_SRC;
#define SGR_POINTER_SMALL_PNG                0
extern void* RefreshClusterTempPointer(SgWidget* w);
extern void CacheClusterTempPointer(SgWidget* w);
extern const SgSRC sgwClusterTempPointer_SRC;
#define SGR_POINTER_SMALL_PNG                0
extern void* RefreshClusterFuelPointer(SgWidget* w);
extern void CacheClusterFuelPointer(SgWidget* w);
extern const SgSRC sgwClusterFuelPointer_SRC;
#define SGR_TT_TPMS_PNG                      0
extern void* RefreshTelltaleTPMS(SgWidget* w);
extern void CacheTelltaleTPMS(SgWidget* w);
extern const SgSRC sgwTelltaleTPMS_SRC;
#define SGR_TT_LOW_OIL_PNG                   0
extern void* RefreshTelltaleLowOil(SgWidget* w);
extern void CacheTelltaleLowOil(SgWidget* w);
extern const SgSRC sgwTelltaleLowOil_SRC;
#define SGR_TT_TURN_RIGHT_PNG                0
extern void* RefreshTelltaleTurnRight(SgWidget* w);
extern void CacheTelltaleTurnRight(SgWidget* w);
extern const SgSRC sgwTelltaleTurnRight_SRC;
#define SGR_TT_TURN_LEFT_PNG                 0
extern void* RefreshTelltaleTurnLeft(SgWidget* w);
extern void CacheTelltaleTurnLeft(SgWidget* w);
extern const SgSRC sgwTelltaleTurnLeft_SRC;
#define SGR_TT_POSLAMP_PNG                   0
extern void* RefreshTelltalePosLamp(SgWidget* w);
extern void CacheTelltalePosLamp(SgWidget* w);
extern const SgSRC sgwTelltalePosLamp_SRC;
#define SGR_TT_AUTO_CRUISE_PNG               0
extern void* RefreshTelltaleAutoCruise(SgWidget* w);
extern void CacheTelltaleAutoCruise(SgWidget* w);
extern const SgSRC sgwTelltaleAutoCruise_SRC;
#define SGR_TT_HIGH_BEAM_PNG                 0
extern void* RefreshTelltaleHighBeam(SgWidget* w);
extern void CacheTelltaleHighBeam(SgWidget* w);
extern const SgSRC sgwTelltaleHighBeam_SRC;
#define SGR_TT_SEATBELT_DRIVER_PNG           0
extern void* RefreshTelltaleSeatbeltDriver(SgWidget* w);
extern void CacheTelltaleSeatbeltDriver(SgWidget* w);
extern const SgSRC sgwTelltaleSeatbeltDriver_SRC;
#define SGR_TT_SEATBELT_PASSENGER_PNG        0
extern void* RefreshTelltaleSeatbeltPassenger(SgWidget* w);
extern void CacheTelltaleSeatbeltPassenger(SgWidget* w);
extern const SgSRC sgwTelltaleSeatbeltPassenger_SRC;
#define SGR_TT_AIRBAG_PNG                    0
extern void* RefreshTelltaleAirbag(SgWidget* w);
extern void CacheTelltaleAirbag(SgWidget* w);
extern const SgSRC sgwTelltaleAirbag_SRC;


#define SGW_SGWCLUSTERBACKGROUND             0
#define SGW_SGWCLUSTERTACHOPOINTER           1
#define SGW_SGWCLUSTERSPEEDPOINTER           2
#define SGW_SGWCLUSTERTEMPPOINTER            3
#define SGW_SGWCLUSTERFUELPOINTER            4
#define SGW_SGWTELLTALETPMS                  5
#define SGW_SGWTELLTALELOWOIL                6
#define SGW_SGWTELLTALETURNRIGHT             7
#define SGW_SGWTELLTALETURNLEFT              8
#define SGW_SGWTELLTALEPOSLAMP               9
#define SGW_SGWTELLTALEAUTOCRUISE            10
#define SGW_SGWTELLTALEHIGHBEAM              11
#define SGW_SGWTELLTALESEATBELTDRIVER        12
#define SGW_SGWTELLTALESEATBELTPASSENGER     13
#define SGW_SGWTELLTALEAIRBAG                14
#define SGW_MAX                              15


extern SgWidget SGWidget[15];



#define SGL_MAX 3

#endif

