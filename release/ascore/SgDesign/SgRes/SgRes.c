
/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2015  AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
 
#include "Sg.h"
SgWidget SGWidget[15] = 
{
	{ /* SGW_SGWCLUSTERBACKGROUND */
		/*x =*/0,
		/*y =*/0,
		/*w =*/1200,
		/*h =*/500,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/0,
		/*ri=*/SGR_BG_PNG,
		/*src =*/&sgwClusterBackground_SRC
	},
	{ /* SGW_SGWCLUSTERTACHOPOINTER */
		/*x =*/340,
		/*y =*/285,
		/*w =*/200,
		/*h =*/16,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/2,
		/*ri=*/SGR_POINTER_BIG_PNG,
		/*src =*/&sgwClusterTachoPointer_SRC
	},
	{ /* SGW_SGWCLUSTERSPEEDPOINTER */
		/*x =*/850,
		/*y =*/285,
		/*w =*/200,
		/*h =*/16,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/2,
		/*ri=*/SGR_POINTER_BIG_PNG,
		/*src =*/&sgwClusterSpeedPointer_SRC
	},
	{ /* SGW_SGWCLUSTERTEMPPOINTER */
		/*x =*/134,
		/*y =*/383,
		/*w =*/80,
		/*h =*/8,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/2,
		/*ri=*/SGR_POINTER_SMALL_PNG,
		/*src =*/&sgwClusterTempPointer_SRC
	},
	{ /* SGW_SGWCLUSTERFUELPOINTER */
		/*x =*/1055,
		/*y =*/383,
		/*w =*/80,
		/*h =*/8,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/2,
		/*ri=*/SGR_POINTER_SMALL_PNG,
		/*src =*/&sgwClusterFuelPointer_SRC
	},
	{ /* SGW_SGWTELLTALETPMS */
		/*x =*/400,
		/*y =*/315,
		/*w =*/53,
		/*h =*/49,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_TPMS_PNG,
		/*src =*/&sgwTelltaleTPMS_SRC
	},
	{ /* SGW_SGWTELLTALELOWOIL */
		/*x =*/220,
		/*y =*/320,
		/*w =*/64,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_LOW_OIL_PNG,
		/*src =*/&sgwTelltaleLowOil_SRC
	},
	{ /* SGW_SGWTELLTALETURNRIGHT */
		/*x =*/810,
		/*y =*/25,
		/*w =*/29,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_TURN_RIGHT_PNG,
		/*src =*/&sgwTelltaleTurnRight_SRC
	},
	{ /* SGW_SGWTELLTALETURNLEFT */
		/*x =*/340,
		/*y =*/25,
		/*w =*/29,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_TURN_LEFT_PNG,
		/*src =*/&sgwTelltaleTurnLeft_SRC
	},
	{ /* SGW_SGWTELLTALEPOSLAMP */
		/*x =*/561,
		/*y =*/25,
		/*w =*/58,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_POSLAMP_PNG,
		/*src =*/&sgwTelltalePosLamp_SRC
	},
	{ /* SGW_SGWTELLTALEAUTOCRUISE */
		/*x =*/625,
		/*y =*/25,
		/*w =*/41,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_AUTO_CRUISE_PNG,
		/*src =*/&sgwTelltaleAutoCruise_SRC
	},
	{ /* SGW_SGWTELLTALEHIGHBEAM */
		/*x =*/497,
		/*y =*/25,
		/*w =*/55,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_HIGH_BEAM_PNG,
		/*src =*/&sgwTelltaleHighBeam_SRC
	},
	{ /* SGW_SGWTELLTALESEATBELTDRIVER */
		/*x =*/448,
		/*y =*/25,
		/*w =*/27,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_SEATBELT_DRIVER_PNG,
		/*src =*/&sgwTelltaleSeatbeltDriver_SRC
	},
	{ /* SGW_SGWTELLTALESEATBELTPASSENGER */
		/*x =*/677,
		/*y =*/25,
		/*w =*/32,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_SEATBELT_PASSENGER_PNG,
		/*src =*/&sgwTelltaleSeatbeltPassenger_SRC
	},
	{ /* SGW_SGWTELLTALEAIRBAG */
		/*x =*/725,
		/*y =*/25,
		/*w =*/38,
		/*h =*/36,
		/*c =*/0,
		/*d =*/0xFFFF/*no rotation*/,
		/*l =*/1,
		/*ri=*/SGR_TT_AIRBAG_PNG,
		/*src =*/&sgwTelltaleAirbag_SRC
	},
};

