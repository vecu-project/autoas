#!/bin/bash

Option=$1
File=./build/posix/socket_lin_driver.exe

set_can(){

modprobe can
modprobe can_raw
modprobe vcan
ip link add dev can0 type vcan
ip link set up can0
echo "Set vcan done"

if [ -f "$File" ]; then  
./build/posix/socket_lin_driver.exe 0
fi

}

del_can(){

ip link del dev can0

echo "Delete vcan done"
}


open_lin_socket(){
echo "Open lin driver done"
if [ -f "$File" ]; then  
./build/posix/socket_lin_driver.exe 0
fi
}




if [ $# -eq 0 ]; then
    echo "No arguments provided"
    echo "-s | --s | --set		start vcan"
    echo "-d | --d | --del		end vcan"
    echo "-o | --o | --open		open lin socket driver"
    exit 1
fi

case $Option in
  -s|--s|--set)
    set_can ;;
  -d|--d|--del)
    del_can ;;
  -o|--o|--open)
    open_lin_socket ;;
 esac
