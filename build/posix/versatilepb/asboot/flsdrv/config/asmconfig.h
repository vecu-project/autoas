#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_CAN
#define USE_CAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_XCP
#define USE_XCP 1
#endif

#ifndef USE_UIP
#define USE_UIP 1
#endif

#ifndef USE_SOAD
#define USE_SOAD 1
#endif

#ifndef USE_DOIP
#define USE_DOIP 1
#endif

#ifndef USE_PROTOTHREAD
#define USE_PROTOTHREAD 1
#endif

#ifndef USE_FLASH
#define USE_FLASH 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_FLASH_CMD
#define USE_FLASH_CMD 1
#endif

#ifndef USE_PCI
#define USE_PCI 1
#endif

#ifndef USE_CLIB_ASHEAP
#define USE_CLIB_ASHEAP 1
#endif

#ifndef USE_CLIB_MISCLIB
#define USE_CLIB_MISCLIB 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_TINYOS
#define USE_TINYOS 1
#endif

#ifndef USE_BOOTLOADER
#define USE_BOOTLOADER 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef FLASH_DRIVER_DYNAMIC_DOWNLOAD
#define FLASH_DRIVER_DYNAMIC_DOWNLOAD
#endif
#ifndef FLS_START_ADDRESS
#define FLS_START_ADDRESS 0x00040000
#endif
#ifndef FLS_END_ADDRESS
#define FLS_END_ADDRESS 0x08000000
#endif
#ifndef FLASH_WRITE_SIZE
#define FLASH_WRITE_SIZE 2
#endif
#endif /* _AS_MCONF_H_ */
