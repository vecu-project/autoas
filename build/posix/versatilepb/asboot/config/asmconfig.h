#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_CAN
#define USE_CAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_XCP
#define USE_XCP 1
#endif

#ifndef USE_UIP
#define USE_UIP 1
#endif

#ifndef USE_SOAD
#define USE_SOAD 1
#endif

#ifndef USE_DOIP
#define USE_DOIP 1
#endif

#ifndef USE_PROTOTHREAD
#define USE_PROTOTHREAD 1
#endif

#ifndef USE_FLASH
#define USE_FLASH 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_FLASH_CMD
#define USE_FLASH_CMD 1
#endif

#ifndef USE_PCI
#define USE_PCI 1
#endif

#ifndef USE_CLIB_ASHEAP
#define USE_CLIB_ASHEAP 1
#endif

#ifndef USE_CLIB_MISCLIB
#define USE_CLIB_MISCLIB 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_TINYOS
#define USE_TINYOS 1
#endif

#ifndef USE_BOOTLOADER
#define USE_BOOTLOADER 1
#endif

#ifndef USE_DIO
#define USE_DIO 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef configTOTAL_HEAP_SIZE
#define configTOTAL_HEAP_SIZE 0x7000
#endif
#ifndef MEMORY_SIZE
#define MEMORY_SIZE 0x8000000
#endif
#ifndef SYSTEM_REGION_START
#define SYSTEM_REGION_START 0x10000000
#endif
#ifndef SYSTEM_REGION_END
#define SYSTEM_REGION_END 0x101f4000
#endif
#ifndef PAGE_SIZE
#define PAGE_SIZE 0x1000
#endif
#ifndef OS_TICKS_PER_SECOND
#define OS_TICKS_PER_SECOND 100
#endif
#ifndef USECONDS_PER_TICK
#define USECONDS_PER_TICK 10000
#endif
#ifndef OS_STK_SIZE_SCALER
#define OS_STK_SIZE_SCALER 4
#endif
#ifndef FL_ERASE_PER_CYCLE
#define FL_ERASE_PER_CYCLE 2048
#endif
#ifndef ipaddr_addr
#define ipaddr_addr inet_addr
#endif
#ifndef UIP_CONF_BUFFER_SIZE
#define UIP_CONF_BUFFER_SIZE 1500
#endif
#ifndef __AS_BOOTLOADER__
#define __AS_BOOTLOADER__
#endif
#ifndef NO_STDIO_RINGBUFFER
#define NO_STDIO_RINGBUFFER
#endif
#ifndef CAN_LL_DL
#define CAN_LL_DL 64
#endif
#ifndef USE_versatilepb
#define USE_versatilepb 1
#endif

#endif /* _AS_MCONF_H_ */
