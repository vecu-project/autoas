/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2015  AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
/* ============================ [ INCLUDES  ] ====================================================== */
#include "Os.h"
/* ============================ [ MACROS    ] ====================================================== */
/* ============================ [ TYPES     ] ====================================================== */
/* ============================ [ DECLARES  ] ====================================================== */
extern void KsmUSB2Serial_Init                 (void);
extern void KsmUSB2Serial_Start                (void);
extern void KsmUSB2Serial_Stop                 (void);
extern void KsmUSB2Serial_Running              (void);
/* ============================ [ DATAS     ] ====================================================== */
static const KsmFunction_Type KsmUSB2Serial_FunctionList[4] = 
{
	KsmUSB2Serial_Init                 ,
	KsmUSB2Serial_Start                ,
	KsmUSB2Serial_Stop                 ,
	KsmUSB2Serial_Running              ,
};
const KSM_Type KSM_Config[KSM_NUM] = 
{
	{ /* USB2Serial */
		4,
		KsmUSB2Serial_FunctionList
	},
};

/* ============================ [ LOCALS    ] ====================================================== */
/* ============================ [ FUNCTIONS ] ====================================================== */
