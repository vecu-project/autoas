#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_USB
#define USE_USB 1
#endif

#ifndef USE_USB_SERIAL
#define USE_USB_SERIAL 1
#endif

#ifndef USE_CLIB_ASHEAP
#define USE_CLIB_ASHEAP 1
#endif

#ifndef USE_CLIB_MBOX
#define USE_CLIB_MBOX 1
#endif

#ifndef USE_CLIB_MISCLIB
#define USE_CLIB_MISCLIB 1
#endif

#ifndef USE_CLIB_STRTOK_R
#define USE_CLIB_STRTOK_R 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_ASKAR
#define USE_ASKAR 1
#endif

#ifndef USE_COMMONXML
#define USE_COMMONXML 1
#endif

#ifndef USE_CLIB_MBOX
#define USE_CLIB_MBOX 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef CHIP_AT91SAM3S
#define CHIP_AT91SAM3S
#endif
#ifndef sam3s4
#define sam3s4
#endif
#ifndef ENABLE_SHELL_ECHO_BACK
#define ENABLE_SHELL_ECHO_BACK
#endif
#ifndef OS_STK_SIZE_SCALER
#define OS_STK_SIZE_SCALER 1/2
#endif
#ifndef USBD_LEDUSB
#define USBD_LEDUSB LED_RED
#endif
#endif /* _AS_MCONF_H_ */
