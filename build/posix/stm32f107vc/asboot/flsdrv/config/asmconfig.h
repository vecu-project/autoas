#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_SCAN
#define USE_SCAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_CLIB_MISCLIB
#define USE_CLIB_MISCLIB 1
#endif

#ifndef USE_TINYOS
#define USE_TINYOS 1
#endif

#ifndef USE_BOOTLOADER
#define USE_BOOTLOADER 1
#endif

#ifndef USE_USB
#define USE_USB 1
#endif

#ifndef USE_USB_CAN
#define USE_USB_CAN 1
#endif

#ifndef USE_CANTP_MINI
#define USE_CANTP_MINI 1
#endif

#ifndef USE_DCM_MINI
#define USE_DCM_MINI 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_FLASH_CMD
#define USE_FLASH_CMD 1
#endif

#ifndef USE_MEM_CMD
#define USE_MEM_CMD 1
#endif

#ifndef USE_JMP_CMD
#define USE_JMP_CMD 1
#endif

#ifndef USE_CLIB_STRTOK_R
#define USE_CLIB_STRTOK_R 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef FLASH_DRIVER_DYNAMIC_DOWNLOAD
#define FLASH_DRIVER_DYNAMIC_DOWNLOAD
#endif
#ifndef FLASH_WRITE_SIZE
#define FLASH_WRITE_SIZE 4
#endif
#ifndef CHIP_STM32F10X
#define CHIP_STM32F10X
#endif
#ifndef STM32F10X_CL
#define STM32F10X_CL
#endif
#ifndef USE_STDPERIPH_DRIVER
#define USE_STDPERIPH_DRIVER
#endif
#endif /* _AS_MCONF_H_ */
