/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2015  AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
/* ============================ [ INCLUDES  ] ====================================================== */
#include "Os.h"
/* ============================ [ MACROS    ] ====================================================== */
#ifndef ISR_ATTR
#define ISR_ATTR
#endif
#ifndef ISR_ADDR
#define ISR_ADDR(isr) isr
#endif
/* ============================ [ TYPES     ] ====================================================== */
/* ============================ [ DECLARES  ] ====================================================== */
extern void ISR_ATTR knl_isr_usart2_process (void);
extern void ISR_ATTR OTG_FS_IRQHandler (void);
/* ============================ [ DATAS     ] ====================================================== */
/* ============================ [ LOCALS    ] ====================================================== */
void ISR_ATTR __weak default_isr_handle(void) { ShutdownOS(0xEE); }
/* ============================ [ FUNCTIONS ] ====================================================== */
CONST(task_declare_t,AUTOMATIC)  TaskList[TASK_NUM] = 
{
	DeclareTask(TaskIdle                        , FALSE, OSDEFAULTAPPMODE    ),
	DeclareTask(TaskShell                       , TRUE , OSDEFAULTAPPMODE    ),
	DeclareTask(SchM_Startup                    , TRUE , OSDEFAULTAPPMODE    ),
	DeclareTask(SchM_BswService                 , FALSE, OSDEFAULTAPPMODE    ),
	DeclareTask(TaskCanIf                       , TRUE , OSDEFAULTAPPMODE    ),
};

CONST(alarm_declare_t,AUTOMATIC) AlarmList[ALARM_NUM] = 
{
	DeclareAlarm(Alarm_BswService),
};

#ifdef __HIWARE__
#pragma DATA_SEG __NEAR_SEG .vectors
const uint16 tisr_pc[ 68 ] = {
#else
const FP tisr_pc[ 68 ] = {
#endif
	ISR_ADDR(default_isr_handle), /* 0 */
	ISR_ADDR(default_isr_handle), /* 1 */
	ISR_ADDR(default_isr_handle), /* 2 */
	ISR_ADDR(default_isr_handle), /* 3 */
	ISR_ADDR(default_isr_handle), /* 4 */
	ISR_ADDR(default_isr_handle), /* 5 */
	ISR_ADDR(default_isr_handle), /* 6 */
	ISR_ADDR(default_isr_handle), /* 7 */
	ISR_ADDR(default_isr_handle), /* 8 */
	ISR_ADDR(default_isr_handle), /* 9 */
	ISR_ADDR(default_isr_handle), /* 10 */
	ISR_ADDR(default_isr_handle), /* 11 */
	ISR_ADDR(default_isr_handle), /* 12 */
	ISR_ADDR(default_isr_handle), /* 13 */
	ISR_ADDR(default_isr_handle), /* 14 */
	ISR_ADDR(default_isr_handle), /* 15 */
	ISR_ADDR(default_isr_handle), /* 16 */
	ISR_ADDR(default_isr_handle), /* 17 */
	ISR_ADDR(default_isr_handle), /* 18 */
	ISR_ADDR(default_isr_handle), /* 19 */
	ISR_ADDR(default_isr_handle), /* 20 */
	ISR_ADDR(default_isr_handle), /* 21 */
	ISR_ADDR(default_isr_handle), /* 22 */
	ISR_ADDR(default_isr_handle), /* 23 */
	ISR_ADDR(default_isr_handle), /* 24 */
	ISR_ADDR(default_isr_handle), /* 25 */
	ISR_ADDR(default_isr_handle), /* 26 */
	ISR_ADDR(default_isr_handle), /* 27 */
	ISR_ADDR(default_isr_handle), /* 28 */
	ISR_ADDR(default_isr_handle), /* 29 */
	ISR_ADDR(default_isr_handle), /* 30 */
	ISR_ADDR(default_isr_handle), /* 31 */
	ISR_ADDR(default_isr_handle), /* 32 */
	ISR_ADDR(default_isr_handle), /* 33 */
	ISR_ADDR(default_isr_handle), /* 34 */
	ISR_ADDR(default_isr_handle), /* 35 */
	ISR_ADDR(default_isr_handle), /* 36 */
	ISR_ADDR(default_isr_handle), /* 37 */
	ISR_ADDR(knl_isr_usart2_process), /* 38 */
	ISR_ADDR(default_isr_handle), /* 39 */
	ISR_ADDR(default_isr_handle), /* 40 */
	ISR_ADDR(default_isr_handle), /* 41 */
	ISR_ADDR(default_isr_handle), /* 42 */
	ISR_ADDR(default_isr_handle), /* 43 */
	ISR_ADDR(default_isr_handle), /* 44 */
	ISR_ADDR(default_isr_handle), /* 45 */
	ISR_ADDR(default_isr_handle), /* 46 */
	ISR_ADDR(default_isr_handle), /* 47 */
	ISR_ADDR(default_isr_handle), /* 48 */
	ISR_ADDR(default_isr_handle), /* 49 */
	ISR_ADDR(default_isr_handle), /* 50 */
	ISR_ADDR(default_isr_handle), /* 51 */
	ISR_ADDR(default_isr_handle), /* 52 */
	ISR_ADDR(default_isr_handle), /* 53 */
	ISR_ADDR(default_isr_handle), /* 54 */
	ISR_ADDR(default_isr_handle), /* 55 */
	ISR_ADDR(default_isr_handle), /* 56 */
	ISR_ADDR(default_isr_handle), /* 57 */
	ISR_ADDR(default_isr_handle), /* 58 */
	ISR_ADDR(default_isr_handle), /* 59 */
	ISR_ADDR(default_isr_handle), /* 60 */
	ISR_ADDR(default_isr_handle), /* 61 */
	ISR_ADDR(default_isr_handle), /* 62 */
	ISR_ADDR(default_isr_handle), /* 63 */
	ISR_ADDR(default_isr_handle), /* 64 */
	ISR_ADDR(default_isr_handle), /* 65 */
	ISR_ADDR(default_isr_handle), /* 66 */
	ISR_ADDR(OTG_FS_IRQHandler), /* 67 */
};

#ifdef __HIWARE__
#pragma DATA_SEG DEFAULT
#endif


