#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_CAN
#define USE_CAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_COM
#define USE_COM 1
#endif

#ifndef USE_COMM
#define USE_COMM 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_CANNM
#define USE_CANNM 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_CANNM
#define USE_CANNM 1
#endif

#ifndef USE_CANSM
#define USE_CANSM 1
#endif

#ifndef USE_NM
#define USE_NM 1
#endif

#ifndef USE_OSEKNM
#define USE_OSEKNM 1
#endif

#ifndef USE_XCP
#define USE_XCP 1
#endif

#ifndef USE_CLIB_MISCLIB
#define USE_CLIB_MISCLIB 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_ASKAR
#define USE_ASKAR 1
#endif

#ifndef USE_COMMONXML
#define USE_COMMONXML 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef OS_TICKS_PER_SECOND
#define OS_TICKS_PER_SECOND 100
#endif
#ifndef USECONDS_PER_TICK
#define USECONDS_PER_TICK 10000
#endif
#ifndef CHIP_LM3S6965
#define CHIP_LM3S6965
#endif
#endif /* _AS_MCONF_H_ */
