import autosar

C_VehicleSpeed_IV = autosar.createConstantTemplateFromPhysicalType('C_VehicleSpeed_IV', autosar.UINT16_T, 0)
C_TachoSpeed_IV = autosar.createConstantTemplateFromPhysicalType('C_TachoSpeed_IV', autosar.UINT16_T, 0)
C_Led1Sts_IV = autosar.createConstantTemplateFromPhysicalType('C_Led1Sts_IV', autosar.UINT8_T, 0)
C_Led2Sts_IV = autosar.createConstantTemplateFromPhysicalType('C_Led2Sts_IV', autosar.UINT8_T, 0)
C_Led3Sts_IV = autosar.createConstantTemplateFromPhysicalType('C_Led3Sts_IV', autosar.UINT8_T, 0)
C_year_IV = autosar.createConstantTemplateFromPhysicalType('C_year_IV', autosar.UINT16_T, 2013)
C_month_IV = autosar.createConstantTemplateFromPhysicalType('C_month_IV', autosar.UINT8_T, 12)
C_day_IV = autosar.createConstantTemplateFromPhysicalType('C_day_IV', autosar.UINT8_T, 15)
C_hour_IV = autosar.createConstantTemplateFromPhysicalType('C_hour_IV', autosar.UINT8_T, 19)
C_minute_IV = autosar.createConstantTemplateFromPhysicalType('C_minute_IV', autosar.UINT8_T, 49)
C_second_IV = autosar.createConstantTemplateFromPhysicalType('C_second_IV', autosar.UINT8_T, 0)

COM_D = []
COM_D.append(autosar.createDataElementTemplate('VehicleSpeed', autosar.UINT16_T))
COM_D.append(autosar.createDataElementTemplate('TachoSpeed', autosar.UINT16_T))
COM_D.append(autosar.createDataElementTemplate('Led1Sts', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('Led2Sts', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('Led3Sts', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('year', autosar.UINT16_T))
COM_D.append(autosar.createDataElementTemplate('month', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('day', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('hour', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('minute', autosar.UINT8_T))
COM_D.append(autosar.createDataElementTemplate('second', autosar.UINT8_T))

COM_I = autosar.createSenderReceiverInterfaceTemplate('Com_I', COM_D)

VehicleSpeed = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_VehicleSpeed_IV, aliveTimeout=30, elemName='VehicleSpeed')
TachoSpeed = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_TachoSpeed_IV, aliveTimeout=30, elemName='TachoSpeed')
Led1Sts = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_Led1Sts_IV, aliveTimeout=30, elemName='Led1Sts')
Led2Sts = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_Led2Sts_IV, aliveTimeout=30, elemName='Led2Sts')
Led3Sts = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_Led3Sts_IV, aliveTimeout=30, elemName='Led3Sts')
year = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_year_IV, aliveTimeout=30, elemName='year')
month = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_month_IV, aliveTimeout=30, elemName='month')
day = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_day_IV, aliveTimeout=30, elemName='day')
hour = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_hour_IV, aliveTimeout=30, elemName='hour')
minute = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_minute_IV, aliveTimeout=30, elemName='minute')
second = autosar.createSenderReceiverPortTemplate('Com', COM_I, C_second_IV, aliveTimeout=30, elemName='second')
