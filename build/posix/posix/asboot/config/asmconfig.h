#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_CAN
#define USE_CAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_XCP
#define USE_XCP 1
#endif

#ifndef USE_FLASH
#define USE_FLASH 1
#endif

#ifndef USE_TINYOS
#define USE_TINYOS 1
#endif

#ifndef USE_CANTP_MINI
#define USE_CANTP_MINI 1
#endif

#ifndef USE_BOOTLOADER
#define USE_BOOTLOADER 1
#endif

#ifndef USE_LUA_CAN
#define USE_LUA_CAN 1
#endif

#ifndef USE_LUA_SOCKET_CAN
#define USE_LUA_SOCKET_CAN 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef OS_TICKS_PER_SECOND
#define OS_TICKS_PER_SECOND 1000000
#endif
#ifndef __AS_BOOTLOADER__
#define __AS_BOOTLOADER__
#endif
#ifndef NO_STDIO_RINGBUFFER
#define NO_STDIO_RINGBUFFER
#endif
#ifndef __AS_CAN_BUS__
#define __AS_CAN_BUS__
#endif
#ifndef CAN_LL_DL
#define CAN_LL_DL 64
#endif
#ifndef __LINUX__
#define __LINUX__
#endif
#endif /* _AS_MCONF_H_ */
