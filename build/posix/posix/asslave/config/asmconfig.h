#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_LINS
#define USE_LINS 1
#endif

#ifndef USE_LINSIF
#define USE_LINSIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_LINSTP
#define USE_LINSTP 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_TINYOS
#define USE_TINYOS 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_LUA_DEV
#define USE_LUA_DEV 1
#endif

#ifndef USE_LUA_DEV_LIN
#define USE_LUA_DEV_LIN 1
#endif

#ifndef USE_LUA_DEV_LIN_SOCKET
#define USE_LUA_DEV_LIN_SOCKET 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef OS_TICKS_PER_SECOND
#define OS_TICKS_PER_SECOND 1000000
#endif
#ifndef __AS_PY_DEV__
#define __AS_PY_DEV__
#endif
#ifndef __LINUX__
#define __LINUX__
#endif
#endif /* _AS_MCONF_H_ */
