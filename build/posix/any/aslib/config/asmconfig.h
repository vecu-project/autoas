#ifndef _AS_MCONF_H_

#ifndef USE_LUA_CAN
#define USE_LUA_CAN 1
#endif

#ifndef USE_LUA_SERIAL_CAN
#define USE_LUA_SERIAL_CAN 1
#endif

#ifndef USE_LUA_SOCKET_CAN
#define USE_LUA_SOCKET_CAN 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_ANYOS
#define USE_ANYOS 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef __LINUX__
#define __LINUX__
#endif
#ifndef __AS_BY_PARAI__
#define __AS_BY_PARAI__
#endif
#ifndef __AS_CAN_BUS__
#define __AS_CAN_BUS__
#endif
#ifndef USE_OSAL
#define USE_OSAL
#endif
#endif /* _AS_MCONF_H_ */
