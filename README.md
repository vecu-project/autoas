## 실행환경 
in Windows,
Env : VMWARE or WSL2  
OS : Ubuntu-22.04  
  
## 의존성 패키지

``` 
$ sudo apt install python2  
$ sudo apt install git  
$ sudo apt install scons  
$ sudo apt install pkg-config  
$ sudo apt-get install libgtk-3-dev  
$ sudo apt install gcc  
$ sudo apt-get install autoconf  
$ sudo apt-get install libtool  
$ sudo apt install make  
$ sudo apt install libc++-14-dev libc++abi-14-dev  
$ sudo apt install g++  

$ sudo apt-get install gtk+-3.0
$ sudo apt install nasm
$ sudo apt install vim
$ sudo apt install dbus-x11
```
  

## 보드 세팅 및 실행

third_party 참조를 위한 path 설정  
```
$ export PYTHONPATH=/home/${USER}/autosar/autoas/com/as.tool/config.infrastructure.system/third_party:${PYTHONPATH} 
```
  
보드세팅 : 현재는 각 레포지토리를 통해 다운 받은 경우 보드 세팅 및 릴리즈 방식 세팅이 되어 있는 상태임

```
$ export BOARD=boardtype  
$ export RELEASE=releasetype  

# 기본
$ export BOARD=posix
$ export RELEASE=ascore
```
  
실행  
```
scons  # 빌드  
# scons --prepare # 의존성 패키지 일괄 설치 
# Ubuntu 환경에서 실행시
1) CAN 소켓 실행 (CAN통신을 위한 소켓 실행 참조조)
2) sudo build/posix/socket_lin_driver.exe 0

scons run # 실행  
```

### POSIX

CAN 통신을 위한 소켓 실행 
- Scons run 하기 전에 실행
```
$ sudo modprobe can               # For WSL2   
$ sudo modprobe can_raw           # WSL2  
  
$ sudo modprobe vcan              # 실패할 경우 dmesg 확인 필요  
  
$ sudo ip link add dev can0 type vcan  
$ sudo ip link set up can0  
$ sudo ip link set can0 mtu 72  

$ sudo socket_lin_driver.exe 0  # /build/posix/socket_lin_driver.exe  

```
 $ sudo modprobe vcan
 $ sudo ip link add dev can0 type vcan
 $ sudo ip link set up can0
 $ sudo ip link set can0 mtu 72


### QEMU 구동을 위한 의존성

x86 및 versatilepb를 지원하기 위한 패키지 설치
```
$ sudo apt install gcc-arm-none-eabi
$ sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386  # 설치 실패 시 하단 참고 확인
```

[참고](https://stackoverflow.com/questions/58681718/intstalling-android-studio-on-64-bit-version-of-lubuntu-fails-installing-some-32)

### 기타 유용한 팀
  
##### CAN 통신 모니터링
```
# can-utils 설치하여 vcan test  
sudo apt install can-utils  

# can0로 아무 신호 생성하여 전송  
cangen can0  

# can0에서 vcan을 통해 나오는 신호 확인  
candump can0

# can0 인터페이스 확인 방법  
ifconfig  # 또는  
ip addr  

# can0 삭제  
sudo ip link del dev can0  
```
  
##### Scons 기타 명령어
```
scons -j$(expr $(nproc) - 1)  # 빠른 빌드를 위한 명령어  
```
  
### QEMU 관련

[다운로드](https://download.qemu.org/qemu-2.10.0.tar.xz)

memfd.c 수정  
```
# line 40 : /qemu-2.10.0/util/util/memfd.c 

# 전
static int memfd_create(const char *name, unsigned int flags)
{
#ifdef __NR_memfd_create
    return syscall(__NR_memfd_create, name, flags);
#else
    return -1;
#endif
}

# 후
int memfd_create(const char *name, unsigned int flags)
{
#ifdef __NR_memfd_create
    return syscall(__NR_memfd_create, name, flags);
#else
    return -1;
#endif
}
```
  
arm-a64.cc 수정  
```
# line 21 : /qemu-2.10.0/disas/arm-a64.cc

# 전

## QEMU 코드 수정 부분
1) qemu-2.10.0/linux-user/syscall.c  

```
--- a/linux-user/syscall.c
+++ b/linux-user/syscall.c
@@ -249,7 +249,8 @@  static type name (type1 arg1,type2 arg2,type3 arg3,type4 arg4,type5 arg5,	\
 #define TARGET_NR__llseek TARGET_NR_llseek
 #endif
 
-_syscall0(int, gettid)
+#define __NR_sys_gettid __NR_gettid
+_syscall0(int, sys_gettid)
 
 /* For the 64-bit guest on 32-bit host case we must emulate
  * getdents using getdents64, because otherwise the host
@@ -5434,7 +5435,7 @@  static void *clone_func(void *arg)
     cpu = ENV_GET_CPU(env);
     thread_cpu = cpu;
     ts = (TaskState *)cpu->opaque;
-    info->tid = gettid();
+    info->tid = sys_gettid();
     task_settid(ts);
     if (info->child_tidptr)
         put_user_u32(info->tid, info->child_tidptr);
@@ -5579,9 +5580,9 @@  static int do_fork(CPUArchState *env, unsigned int flags, abi_ulong newsp,
                mapping.  We can't repeat the spinlock hack used above because
                the child process gets its own copy of the lock.  */
             if (flags & CLONE_CHILD_SETTID)
-                put_user_u32(gettid(), child_tidptr);
+                put_user_u32(sys_gettid(), child_tidptr);
             if (flags & CLONE_PARENT_SETTID)
-                put_user_u32(gettid(), parent_tidptr);
+                put_user_u32(sys_gettid(), parent_tidptr);
             ts = (TaskState *)cpu->opaque;
             if (flags & CLONE_SETTLS)
                 cpu_set_tls (env, newtls);
@@ -10621,7 +10622,7 @@  static abi_long do_syscall1(void *cpu_env, int num, abi_long arg1,
         return TARGET_PAGE_SIZE;
 #endif
     case TARGET_NR_gettid:
-        return get_errno(gettid());
+        return get_errno(sys_gettid());
 #ifdef TARGET_NR_readahead
     case TARGET_NR_readahead:
 #if TARGET_ABI_BITS == 32

```


2) qemu-2.10.0/util/memfd.c  
```
--- a/util/memfd.c
+++ b/util/memfd.c

@@ -37,7 +37,7 @@
 #include <sys/syscall.h>
 #include <asm/unistd.h>
 
-static int memfd_create(const char *name, unsigned int flags)
+int memfd_create(const char *name, unsigned int flags)
 {
 #ifdef __NR_memfd_create
     return syscall(__NR_memfd_create, name, fla
```

3) qemu-2.10.0/disas/arm-a64.cc
```
--- a/disas/arm-a64.cc
+++ b/disas/arm-a64.cc

+#include "vixl/a64/disasm-a64.h"

extern "C" {
#include "qemu/osdep.h"
#include "disas/bfd.h"
}
-#include "vixl/a64/disasm-a64.h"
```

4) qemu-2.10.0/include/ui/egl-helpers.h
```
--- a/include/ui/egl-helpers.h
+++ b/include/ui/egl-helpers.h

#include <epoxy/gl.h>
#include <epoxy/egl.h>
#include <gbm.h>
+#include <X11/Xlib.h>
```


## ASONE 실행 방법

1) aslib 빌드  
```
BOARD=any
ANY=aslib
RELEASE=ascore
scons
```
2) pybind11 사용 설정 및 pyas 빌드
```
BOARD=any
ANY=pyas
RELEASE=ascore
scons --menuconfig # pybind11에 * 체크 후 저장 > 나가기
scons
```
3) asone 실행
```
scons run asone
또는
python com/as.tool/as.one.py/main.py
```

## Studio 사용법
1) 보드설정
```
BOARD=targetboard
RELEASE=ascore
```
2) 스튜디오 실행
```
scons studio
```
